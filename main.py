import asyncio
import logging
from contextlib import suppress
from os import getenv
from random import randint
from sys import exit
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters import Text
from aiogram.utils.exceptions import BotBlocked, MessageNotModified
import aiogram.utils.markdown as fmt


# Объект бота
bot_token = getenv("BOT_TOKEN")
if not bot_token:
    exit("Error: no token provided")

bot = Bot(token=bot_token)
# Диспетчер для бота
dp = Dispatcher(bot)
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands="start")
async def cmd_start(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    buttons = [questions[i][1], questions[i][2], questions[i][3], questions[i][4]]
    keyboard.add(*buttons)
    await message.answer(f"{fmt.hide_link(ssilka[i])}{questions[i][0]}", parse_mode=types.ParseMode.HTML, reply_markup=keyboard)

@dp.message_handler(lambda message: message.text == questions[i][answers[i]])
async def right_answer(message: types.Message):
    global i, verno
    verno += 1
    if verno < 5:
        rezult = rez[0]
    elif verno < 7:
        rezult = rez[1]
    elif verno < 9:
        rezult = rez[2]
    else:
        rezult = rez[3]
    if i < 9:
        i += 1
        await message.reply('Верный ответ!')
        await cmd_start(message)
    else:
        i = 0
        await message.reply('Правильно! Вы ответили верно на '+str(verno)+' вопросов из 10. '+rezult, reply_markup=types.ReplyKeyboardRemove())
        verno = 0

@dp.message_handler(lambda message: message.text != questions[i][answers[i]])
async def wrong_answer(message: types.Message):
    global i, verno
    if verno < 5:
        rezult = rez[0]
    elif verno < 7:
        rezult = rez[1]
    elif verno < 9:
        rezult = rez[2]
    else:
        rezult = rez[3]
    if i < 9:
        i += 1
        await message.reply('Неверный ответ! Верный ответ: '+questions[i-1][answers[i-1]]+'.')
        await cmd_start(message)
    else:
        i = 0
        await message.reply('Неправильно! Верный ответ: '+questions[i-1][answers[i-1]]+'. Вы ответили верно на '+str(verno)+' вопросов из 10. '+rezult, reply_markup=types.ReplyKeyboardRemove())
        verno = 0

if __name__ == "__main__":
    # Запуск бота
    questions = [['Из какой игры пришло выражение «быть в дамках»?', 'прятки', 'шахматы', 'шашки', 'бильярд'],
                 ['Название какого дня недели буквально переводится на японский язык как «день луны»?', 'понедельник', 'пятница', 'суббота', 'воскресенье'],
                 ['Какой сок собирают весной?', 'рябиновый', 'облепиховый', 'яблочный', 'берёзовый'],
                 ['О какой республике её жители говорят так: «Камни, реки, лес и я»?', 'Северная Осетия', 'Карелия', 'Тыва', 'Алтай'],
                 ['Сколько человек в футбольной команде?', '7', '11', '15', '22'],
                 ['Какое прозвище за годы своего правления получил отец Петра I Алексей Михайлович Романов?', 'Благословенный', 'Тишайший', 'Мученик', 'Величайший'],
                 ['Какой меч, по легенде, висел на конском волосе?', 'Дамоклов меч', 'меч короля Артура', 'меч-кладенец', 'меч Святого Петра'],
                 ['Олицетворением какого смертного греха считался Бегемот, демон из христианской мифологии?', 'гнев', 'чревоугодие', 'алчность', 'зависть'],
                 ['Этот человек после смерти стал строгим и неумолимым судьей в подземном царстве Аида. Ничего странного в такой суровости нет, ведь его жена наставила рога не только мужу, но и сыну. А где жил этот сын?', 'в тюрьме', 'в лабиринте', 'на горе Олимп', 'в замке с драконом'],
                 ['Что в записи шахматных партий обозначает знак «+»?', 'шах', 'мат', 'ничью', 'рокировку'],]
    answers = [3, 1, 4, 2, 2, 2, 1, 2, 2, 1]
    verno = 0
    i = 0
    rez = ['Возможно, сегодня вам больше понравится тест на логику?',
           'Неплохо, сразу видно - вы многим интересуетесь, так держать!',
           'Хороший результат! Сразу видно - вы многим интересуетесь, так держать!',
           'Отличный результат! Сразу видно - вы многим интересуетесь, так держать!']
    ssilka = ['https://dropi.ru/img/uploads/test/2022-03-18/898dfe7164372b08d56e8d4a67bb6eb7.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/98a4d7b3e78ebd7d9136b7167dd4326a.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/00d60c1940a453cc3bbd7a9a7327fa6a.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/1181225c2c594351db5ad2bc5d54f648.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/48c85d55b123cb60dfbbfc12fb0dfacb.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/84fe3fe4f62e07834dcee996b6f16264.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/6985036b7a4f0a7ceae5b6d4c431456d.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/4de6dde3c54e6f879d4ac6afec4a33a4.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/f66e042ff505907445a67624a6866a64.jpeg',
              'https://dropi.ru/img/uploads/test/2022-03-18/8ae6320b2fe02689f66608d8536461cc.jpeg']
    executor.start_polling(dp, skip_updates=True)


















































#@dp.message_handler(commands="start")
#async def cmd_start(message: types.Message):
   # keyboard = types.ReplyKeyboardMarkup()
    #button_1 = types.KeyboardButton(text="С пюрешкой")
   # keyboard.add(button_1)
   # button_2 = "Без пюрешки"
   # keyboard.add(button_2)
   # await message.answer("Как подавать котлеты?", reply_markup=keyboard)

#@dp.message_handler(commands="block")
#async def cmd_block(message: types.Message):
    #await asyncio.sleep(10.0)  # Здоровый сон на 10 секунд
    #await message.reply("Вы заблокированы")

#@dp.errors_handler(exception=BotBlocked)
#async def error_bot_blocked(update: types.Update, exception: BotBlocked):
    # Update: объект события от Telegram. Exception: объект исключения
    # Здесь можно как-то обработать блокировку, например, удалить пользователя из БД
    #print(f"Меня заблокировал пользователь!\nСообщение: {update}\nОшибка: {exception}")

    # Такой хэндлер должен всегда возвращать True,
    # если дальнейшая обработка не требуется.
    #return True


# Хэндлер на команду /test1
#@dp.message_handler(commands="test1")
#async def cmd_test1(message: types.Message):
    #await message.reply("Test 1")

#async def cmd_test2(message: types.Message):
  #  await message.reply("Test 2")
#dp.register_message_handler(cmd_test2, commands="test2")

#@dp.message_handler(commands="answer")
#async def cmd_answer(message: types.Message):
   # await message.answer(
      #  fmt.text(
          #  fmt.text(fmt.hunderline("Яблоки"), ", вес 1 кг."),
          #  fmt.text("Старая цена:", fmt.hstrikethrough(50), "рублей"),
          #  fmt.text("Новая цена:", fmt.hbold(25), "рублей"),
          #  sep="\n"
    #    ), parse_mode="HTML"
  #  )



#@dp.message_handler(content_types=[types.ContentType.ANIMATION])
#async def echo_document(message: types.Message):
   # await message.reply_animation(message.animation.file_id)



#@dp.message_handler(commands="test4")
#async def with_hidden_link(message: types.Message):
    #await message.answer(
        #f"{fmt.hide_link('https://telegram.org/blog/video-calls/ru')}Кто бы мог подумать, что "
        #f"в 2020 году в Telegram появятся видеозвонки!\n\nОбычные голосовые вызовы "
        #f"возникли в Telegram лишь в 2017, заметно позже своих конкурентов. А спустя три года, "
        #f"когда огромное количество людей на планете приучились работать из дома из-за эпидемии "
        #f"коронавируса, команда Павла Дурова не растерялась и сделала качественные "
        #f"видеозвонки на WebRTC!\n\nP.S. а ещё ходят слухи про демонстрацию своего экрана :)",
        #parse_mode=types.ParseMode.HTML)

#@dp.message_handler(commands="reply")
#async def cmd_reply(message: types.Message):
   # await message.reply('Это ответ с "ответом"')


#@dp.message_handler(commands="dice")
#async def cmd_reply(message: types.Message):
   # await message.reply("🎲")



